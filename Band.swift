//  Band.swift -- A set of Keyboards, each with a different scale C2017PAK

import Foundation
import UIKit

let singletonBand: Band = Band(bobbytrap:false)

class Band {
	class func the() -> Band {
		return singletonBand
	}

	let prototypes : [Int:[Int]] = [
		0:[],				// degenerate
		1:[0],
		2:[1],
		3:[1,0],
		4:[1,1],
		5:[1,1,0],
		6:[1,1,0,0],
		7:[0,0,0,0,0,0,0],		//7:[1,1,1,0], //7:[1,1,0,1],
		8:[1,1,0,1,0],
		9:[1,1,0,1,1],
		10:[1,1,0,1,1,0],
		11:[1,1,0,1,1,1],		//11:[1,1,0,1,1,1], //11:[1,1,1,1,1,0],
		12:[1,1,0,1,1,1,0],
		13:[1,1,0,1,1,0,1,0],
		14:[1,1,0,1,1,0,1,0],	// DESIGN
		15:[1,1,0,1,1,0,1,0],	// DESIGN
	]

	init(bobbytrap:Bool) {
		if bobbytrap {
			fatalError("Access not using " +
				"'Band.the()' constructor. 'Band() alone does not access the singleton' !")
		}
	}
	
	var synth						= Synthesizer()		// (sound system)
	var soloKeyboard	:Keyboard?	= nil
	
	func pickSoloKeyboard(keyboardNumber:Int) {
		synth.stopEngine()
		
		let k						= keyboardNumber
		soloKeyboard = Keyboard(halfsPOctave:k, blackMask:prototypes[k]!)
		
		synth.startEngine()

//		var rv		 :Keyboard?		= nil
//		if k==0  { rv=Keyboard(n:0,  m:[]) }	// degenerate
//		if k==1  { rv=Keyboard(n:1,  m:[0]) }
//		if k==2  { rv=Keyboard(n:2,  m:[1]) }
//		if k==3  { rv=Keyboard(n:3,  m:[1,0]) }
//		if k==4  { rv=Keyboard(n:4,  m:[1,1]) }
//		if k==5  { rv=Keyboard(n:5,  m:[1,1,0]) }
//		if k==6  { rv=Keyboard(n:6,  m:[1,1,0,0]) }
//		if k==7  { rv=Keyboard(n:7,  m:[1,1,1,0]) }
////		if k==7  { rv=Keyboard(n:7,  m:[1,1,0,1]) }
//		if k==8  { rv=Keyboard(n:8,  m:[1,1,0,1,0]) }
//		if k==9  { rv=Keyboard(n:9,  m:[1,1,0,1,1]) }
//		if k==10 { rv=Keyboard(n:10, m:[1,1,0,1,1,0]) }
//			//        Keyboard(n:11, m:[1,1,0,1,1,1]) }
//		if k==11 { rv=Keyboard(n:11, m:[1,1,0,1,1,1]) }
////		if k==11 { rv=Keyboard(n:11, m:[1,1,1,1,1,0]) }
//		if k==12 { rv=Keyboard(n:12, m:[1,1,0,1,1,1,0]) }
//		if k==13 { rv=Keyboard(n:13, m:[1,1,0,1,1,0,1,0]) }
//		if k==14 { rv=Keyboard(n:14, m:[1,1,0,1,1,0,1,0]) }	// DESIGN
//		if k==15 { rv=Keyboard(n:15, m:[1,1,0,1,1,0,1,0]) }	// DESIGN
	}
}

// generate all keyboards


