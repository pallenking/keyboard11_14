//  ViewController.swift -- controls main screen C2017PAK

import AVFoundation
import UIKit

class ViewController: UIViewController {
	
	override func viewDidLoad() {						super.viewDidLoad()

		/// Make the Band:
		Band.the().pickSoloKeyboard(keyboardNumber:12)// keyboards built after singletonBand set up

	}

	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print(#function)
		for sensedTouch:UITouch in touches {
			let loc:CGPoint			= sensedTouch.location(in:nil)

			// Find the Key on Keyboard touched
			let kbd = Band.the().soloKeyboard!
			if let key = kbd.keyAt(point:loc) {
                print("key:\(key.name) -- began")
				key.tone.sound()
				key.playing			= 1						// mostly for visuals
				key.downPoint		= loc
				key.curPoint		= loc
				self.view.setNeedsDisplay()
			}
		}
	}
	override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        print(#function)
        guard let soloKeyboard		= Band.the().soloKeyboard else {
            print("No solo keyboard")
            return
        }
		
		for sensedTouch:UITouch in touches {
			let prevLoc				= sensedTouch.previousLocation(in:nil)  // SYSTEM
			let loc					= sensedTouch.location(in:nil)
            guard let prevKey       = soloKeyboard.keyAt(point:prevLoc) else {
                print("prevKey not available")
                continue
            }
                // If no current key, note is done, silence previous.
            guard let key            = soloKeyboard.keyAt(point:loc) else {
                print("key not available")
                prevKey.tone.silence()
                prevKey.playing     = 0
                prevKey.downPoint   = CGPoint.zero
                prevKey.curPoint    = CGPoint.zero
                self.view.setNeedsDisplay()
                continue                            // done with this touch
            }
            print("key:\(key.name) -- moved")

			if key.frequency != prevKey.frequency {

				 // Silence previous key
				prevKey.tone.silence()
				prevKey.playing	= 0
				prevKey.downPoint	= CGPoint.zero
				prevKey.curPoint	= CGPoint.zero

				 // Sound new key
				key.tone.sound()
				key.playing		= 1
				key.downPoint		= loc
			}
			key.curPoint			= loc
			
			let cx					= Float((key.curPoint.x))
			let cy					= Float((key.curPoint.y))
			let dx					= Float((key.downPoint.x))
			let dy					= Float((key.downPoint.y))
			let tone				= key.tone
			if (true) {
				let bendz			= (dx - cx) / 250.0 + 1.0
				tone.bend			= bendz
			} else {
				let bendx			= (dx - cx) / 25.0 + 1.0
				let bendy			= (dy - cy) / 25.0 + 1.0
				tone.modulatorAmplitude = bendx
				tone.modulatorFrequency = bendy
				print("bendx=\(bendx), bendy=\(bendy)")
			}
			self.view.setNeedsDisplay()
		}
	}
	override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        print(#function)
        let band = Band.the()
        guard let keyboard = band.soloKeyboard,
              let keys = keyboard.keys else {
            print("no keyboard/keys")
            return
        }
		for sensedTouch:UITouch in touches {
			let loc:CGPoint			= sensedTouch.location(in:nil)

			// Find Key on Keyboard released
			if let key:Key = Band.the().soloKeyboard!.keyAt(point:loc) {
                print("key:\(key.name) -- silence")
				key.tone.silence()
				key.playing			= 0		// mostly for visuals
				key.downPoint		= CGPoint.zero
				key.curPoint		= CGPoint.zero
				self.view.setNeedsDisplay()
			}
 		}
	}
	
	@IBAction func minusBlack(_ sender: Any) {
		print("----\n")
	}

	@IBAction func plusBlack(_ sender: UIButton) {
		print("++++\n")
	}

	@IBAction func radioButton(_ sender: UISegmentedControl) {
		let title = sender.titleForSegment(at:sender.selectedSegmentIndex)
		let nSteps: Int = Int(title!)!

		Band.the().pickSoloKeyboard(keyboardNumber:nSteps)
		
		view.setNeedsDisplay()
	}
}

import AVFoundation

///http://www.tmroyal.com/playing-sounds-in-swift-audioengine.html :
class SinePlayer{
	// store persistent objects
	var engine:AVAudioEngine
	var player:AVAudioPlayerNode
	var mixer:AVAudioMixerNode
	var buffer:AVAudioPCMBuffer
	
	init(freq:Float){
		 // initialize objects
		engine				= AVAudioEngine()
		mixer				= engine.mainMixerNode;
		let sampleRate		= Float(mixer.outputFormat(forBus: 0).sampleRate)
		let channelCount	=       mixer.outputFormat(forBus: 0).channelCount
		let freq2 = Float(10)
		
		 // generate sine wave:
		player				= AVAudioPlayerNode()
		buffer				= AVAudioPCMBuffer(pcmFormat:player.outputFormat(forBus: 0), frameCapacity:100)!
		buffer.frameLength	= 1000
		for i in stride(from:0, to:Int(buffer.frameLength), by:Int(channelCount)) {
			let sample		= Float(i)/Float(channelCount)
			let val			= sinf(2.0*Float.pi  * freq2 * sample/sampleRate)
			buffer.floatChannelData!.pointee[i] = val * 0.5
		}
		
		 // setup audio engine:
		engine.attach(player)
		engine.connect(player, to: mixer, format: player.outputFormat(forBus: 0))
		try? engine.start()
		
		 // play player and buffer:
		player.scheduleBuffer(buffer, at:nil, options:.loops, completionHandler:nil)
		player.play()
	}
}

//var sp = SinePlayer(freq:256)
