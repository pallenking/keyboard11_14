//  Etcetera.swift  -- etcetera

import Foundation
import UIKit

extension CGPoint {
	func print()->String {		// Better printing of CGPoint
		return String(format:"(%.2f, %.2f)", self.x, self.y)
	}
}
//print("Button pressed 👍")


// AQDefaultDevice  skipping input stream
// https://stackoverflow.com/questions/40389308/strange-aqdefaultdevice-logging
//				let oscillator = AKOscillator()
//				AudioKit.output = oscillator
//				AudioKit.start()
//				oscillator.start()



/// future:
//	let value = UIInterfaceOrientation.LandscapeLeft.rawValue
//	UIDevice.currentDevice().setValue(value, forKey: "orientation")
////		let tapAlert = UIAlertController(title: "hmmm...", message: "this actually worked?", preferredStyle: UIAlertControllerStyle.alert)
////		tapAlert.addAction(UIAlertAction(title: "OK", style: .destructive, handler: nil))
////		self.present(tapAlert, animated: true, completion: nil)

/*
Proportional Scales
Most music is based on scales
where the ration between two successive notes
is the same
The half notes on a piano
form a proportional scale
with 12 notes per octave.
*/

/*Western Music is based on
a palate of 12 half-notes,
which divide an octave equally.

BUT what if

an octave were divided into
say 13 equally spaced "half"-notes,
or any other number,
2 through 20?

What would music
played on such a non-12 keyboard
sound like?

This program offers a musician
the ability to generate music
from such a scale.
*/
