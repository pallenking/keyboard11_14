//  KeyboardView.swift -- View the keyboard PAK 170619

import UIKit

class KeyboardView: UIView {
							  //// DRAW ////
    override func draw(_ dirtyRect: CGRect) {			super.draw(dirtyRect)

		let kbd = Band.the().soloKeyboard
		kbd?.draw(dirtyRect)
	}
}
