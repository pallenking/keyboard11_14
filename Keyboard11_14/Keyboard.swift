//  Keyboard.swift -- a set of black and white keys in a pattern C2017PAK

import Foundation
import UIKit

   /////////////////////// KEYBOARD ///////////////////////
  /// A Keyboard has black and white keys arranged in a pattern
 ///	The pattern for keyboards with 12 halftones per octave is well known.
///		There are many patterns for (for instance) 11 halftones per octave.

class Keyboard {
	
	var halfsPOctave:Int			// ARG1
	var blackMask	:[Int]			// ARG2 Does the note have a black neighbor?
	let whitesPOctave:Int

	let mag			:CGFloat		= 1.5//2.0//0.5
	let whiteWidth	:CGFloat
	let whiteHeight	:CGFloat
	var outline		:CGRect
	let nOctaves	:Int			= 2
	
	var keys		:[Key]?			= nil

	convenience init(n:Int, m:[Int]) {
		self.init(halfsPOctave:n, blackMask:m)
	}
	
	init(halfsPOctave:Int, blackMask:[Int]) {
		self.halfsPOctave			= halfsPOctave
		self.blackMask				= blackMask

		whitesPOctave				= blackMask.count
		whiteWidth					= CGFloat( 30.0 * mag)
		whiteHeight					= CGFloat(100.0 * mag)
		let width					= whiteWidth  * CGFloat(whitesPOctave) * CGFloat(nOctaves)+20
		let height					= whiteHeight + 30 + 20
		outline						= CGRect(x:10, y:110, width:width, height:height)

		self.build()
	}

	func build() {
		let whitesPOctave:Int		= blackMask.count
		var whiteNames				= ["C", "D", "E", "F", "G", "H", "I", "J", "-", "-"]
		whiteNames[whitesPOctave-2]	= "A"
		whiteNames[whitesPOctave-1]	= "B"
		keys						= []

		
		       ///// Divide an octave into halfsPOctave (e.g. 12) parts x, so that
		      ////    note i has frequency x ^^ i		(x^^y means x to the power y)
		     //		x ^^ halfsPOctave = 2
		    //		ln(x ^^ halfsPOctave) = ln(2)
		   //		halfsPOctave * ln(x)  = -1
		  //		ln(x) = -1.0/halfsPOctave
		 //			x = 2 ^^ (-1.0/halfsPOctave)
		let halfnoteMultiplier	= exp2(1.0/Float(halfsPOctave))

		
		    /// Make a KEYBOARD:
		   /// Segregating (white, then black) allows some required functionality (see below)
		  /// By putting all white notes first, it is easy to prioritize white and black notes
		
		 /// First make WHITE key
		var whiteShape          = CGRect(x:outline.origin.x+10, y:outline.origin.y+10, width:whiteWidth, height:whiteHeight)
		var frequency  :Float	= 256.0			// Middle C
		for whiteI in 0..<(nOctaves*whitesPOctave) { // loop through WHITE notes
			let i = whiteI % whitesPOctave
			let majorName = whiteNames[i]
			
			/// White Note
			let key = Key(name:majorName, frequency:frequency, black:false, rect:whiteShape)
			keys!.append(key)			/// #### REMEMBER WHITE NOTE ###

			/// On to next pitch (skip black keys)
			frequency = frequency * halfnoteMultiplier
			if (blackMask[i] != 0) {
				frequency = frequency * halfnoteMultiplier
			}
			/// Next is to the right
			whiteShape .origin.x += whiteWidth
		}

		
		/// Last make BLACK key
		frequency				= 256.0			// Middle C
		let blackWidth :CGFloat	= whiteWidth * CGFloat(whitesPOctave) / CGFloat(halfsPOctave)
		var blackShape          = CGRect(x:outline.origin.x+10, y:outline.origin.y+10, width:blackWidth,    height:whiteHeight/2)
		 // bias for black notes:   0  1  2  3  4  5  6  7  8  9 10 11 12 14 15 (hack)
		blackShape.origin.x		+= [0, 0, 0, 0, 0, 0, 0, 0, 0,10, 0,20, 0, 0, 0, 0, 0, 0 ][self.halfsPOctave]
		for whiteI in 0..<(nOctaves*whitesPOctave) {	// loop through WHITE notes
			let i = whiteI % whitesPOctave
			let majorName = whiteNames[i]
			
			frequency			*= halfnoteMultiplier	// skip over white note first
			blackShape.origin.x += blackWidth
			if (blackMask[i] != 0) {

				let key = Key(name:majorName+"s", frequency:frequency, black:true, rect:blackShape)
				key.frequency		= frequency
				keys!.append(key)			/// #### REMEMBER BLACK NOTE ###

				/// On to next pitch (skip black keys)
				frequency		*= halfnoteMultiplier
				blackShape.origin.x += blackWidth
			}
		}
	}

	func keyAt(point:CGPoint) -> Key? {				/// find Key pressed
		var keyPressed :Key? = nil		// must use indices for array modification
		for key in keys! {
			if (key.rectangle.contains(point)) {
				keyPressed = key		// remember last: gives black priority
			}
		}
		return keyPressed
	}

	func draw(_ dirtyRect: CGRect) {

		UIColor(red:0.8, green:0.8, blue:0.5, alpha:1.0).set()
//		UIColor.blue.set()
		UIBezierPath(rect:outline).fill()
	
		for index in keys!.indices {
			let key = keys![index]

			key.draw(dirtyRect)
		}
	}
}
