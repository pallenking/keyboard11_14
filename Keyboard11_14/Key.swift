//  Key.swift -- A pitch generator with a black or white actuator C2017

import Foundation
import AVFoundation
import UIKit

class Key {

	///////////////////////// KEY //////////////////////////////
	/// A Key is a mechanism to make a note of a particular pitch.
	/// There is often one Key (aka Key) per pitch.
	///
	init(name:String, frequency:Float, black:Bool, rect:CGRect) {
		self.name				= name
		self.frequency			= frequency
		self.rectangle			= rect
		self.black				= black

		// Make sound player for note:
		let synth				= Band.the().synth
		self.tone				= synth.makeTone(frequency)
	}
	var name        :String
	var rectangle   :CGRect				// place on keyBOARD
	var black       :Bool
	var frequency	:Float		= 0.0
	var downPoint	:CGPoint	= CGPoint.zero
	var curPoint	:CGPoint	= CGPoint.zero
//	var bend		:CGFloat	= 1.0
	var tone		:Tone					// audio generator
	
	var playing					= 0

	func draw(_ dirtyRect: CGRect) {
		let path = UIBezierPath(rect:rectangle)
		path.lineWidth = 2.0
		
		if playing != 0 {			/// note is playing
			UIColor(red:0.4, green:0.4, blue:0.4, alpha:1).set()
			path.fill()					//
			if !black {
				UIColor.black.set()
				path.stroke()              // White note
			}
			self.playing		= 0
		}
		else {						/// note is not playing
			if self.black {
				UIColor.black.set()
				path.fill()                // Black note
			} else {
				UIColor.white.set()
				path.fill()                // Black note
				UIColor.black.set()
				path.stroke()              // White note
			}
		}
		if name == "C" {			/// Octave Marker
			let path = UIBezierPath()
			path.lineWidth		= 6.0
			let o				= rectangle.origin
			path.move(   to: CGPoint(x:o.x, y:o.y - 10))
			path.addLine(to: CGPoint(x:o.x, y:o.y + 10 + 30 + rectangle.size.height))
			path.stroke()
		}
		
		 // Draw name on key
		let r			= self.rectangle			//			  v-- hack
		let textStart	= CGPoint(x:r.origin.x + r.size.width/2 - 5, y:r.origin.y + r.size.height + 5)
		if (!self.black) {
			self.name.draw(at:textStart, withAttributes:[:])
		}

		 /// draw touch spot
		if (curPoint.x != 0) {
			let curPath = UIBezierPath(arcCenter:curPoint, radius:10, startAngle:0.0, endAngle:CGFloat.pi*2, clockwise:true)
			UIColor.red.set()
			curPath.fill()
		}
		if (downPoint.x != 0) {
			let downPath = UIBezierPath(arcCenter:downPoint, radius:15, startAngle:0.0, endAngle:CGFloat.pi*2, clockwise:true)
			UIColor.orange.set()
			downPath.lineWidth		= 2.0
			downPath.stroke()
		}
	}
	
	func print() -> String {
		return "key \(self.name): black=\(black), frequency=\(frequency)"
	}
}
