//  Sound.swift -- AV Sound Playing System PAK2017
//  https://stackoverflow.com/questions/25704923/using-apples-new-audioengine-to-change-pitch-of-audioplayer-sound

import Foundation
import AVFoundation

class Synthesizer {

	let engine				= AVAudioEngine()		// The audio engine manages the sound system.
	let fmt					: AVAudioFormat

	init() {
//		fmt					= engine.mainMixerNode.outputFormat(forBus:0)
		fmt					= AVAudioFormat(standardFormatWithSampleRate:44100.0,
		   						                channels:1)!	// Use standard non-interleaved PCM audio.
		NotificationCenter.default.addObserver(self, selector: #selector(Synthesizer.audioEngineConfigurationChange(_:)), name: NSNotification.Name.AVAudioEngineConfigurationChange, object: engine)
	}
	@objc  func audioEngineConfigurationChange(_ notification: Notification) -> Void {
		NSLog("Audio engine configuration change: \(notification)")
		fatalError("unexpected")
	}
	
	func makeTone(_ frequency:Float32) -> Tone {
		let tone			= Tone(synth:self, frequency:frequency)
		tone.synth			= self
		return tone
	}

	func startEngine() {
		try? engine.start()
	}
	func stopEngine() {
		engine.stop()
	}
	
}

class Tone {
	var frequency			= Float(0)
	var bend				= Float(1)
	var modulatorAmplitude	= Float(0)		// 1.0 10.0 5.0
	var modulatorFrequency	= Float(0)		// 2.0  3.0

	var synth				: Synthesizer

	init(synth:Synthesizer, frequency:Float) {
		self.frequency		= frequency
		self.synth			= synth
		
		playNextBuffer		= DispatchSemaphore(value:bufferPoolSize)	// init semaphore
		for _ in 0..<bufferPoolSize {
			bufferPool.append(AVAudioPCMBuffer(pcmFormat:synth.fmt, frameCapacity:samplesPerBuffer)!)
		}

		/// A SWIFT BUG?  One element is created, and repeated in buferPool!
//		bufferPool			= [AVAudioPCMBuffer](repeating:
//			AVAudioPCMBuffer(pcmFormat:synth.fmt, frameCapacity:samplesPerBuffer),
//												 count:bufferPoolSize)
		
		synth.engine.attach(player)		// Attach and connect the player node.
		synth.engine.connect(player, to:synth.engine.mainMixerNode, format:synth.fmt)
	}
	
	let player				= AVAudioPlayerNode()	// The player node schedules the playback of the audio buffers.
	var time		: Float	= 0						// [[might make global]]
	var phase		: Float = 0						// of output sine wave
	var modPhase	: Float = 0						// of modulator

	var pushing				= false
	var ramp		: Float	= 0
	
	let playNextBuffer		: DispatchSemaphore		//  buffers processed.
	let playQueue			= DispatchQueue(label: "SynthesizerQueue", attributes:[])
								// ^-- The dispatch queue to render audio samples.
	var bufferPool			= [AVAudioPCMBuffer]()	// A circular queue of audio buffers.
	var bufferPoolSize: Int = 3			 			// play one buffer, load next
	var bufferIndex   : Int	= 0						// The index of the next buffer to fill.

	let samplesPerBuffer	: AVAudioFrameCount =  512	// 512 --> 12ms/sample (also 1024)

	func sound() {
		self.pushing					= true			// Pushing --> amplitude increasing
		self.ramp						= 0.0001		// Start not quite at 0, and ramp up
		player.play()

		print("Play frequency \(frequency)")
		
		playQueue.async {
			while true {
				//print("enter ======= ")
				var dRamp:Float			= 0.0001//0.001
				dRamp					= self.pushing ? dRamp : -dRamp
				if self.ramp <= 0 {
					return					// finished last buffer of note; done!
				}
				
				 // Wait for a buffer to become available:
				_ = self.playNextBuffer.wait(timeout:DispatchTime.distantFuture)
				let audioBuffer			= self.bufferPool[self.bufferIndex]
				
				 // Fill in the buffer with new samples.
				let leftChannel			= audioBuffer.floatChannelData?[0]
				let rightChannel		= audioBuffer.floatChannelData?[1]

				let unitVelocity		= Float32(2.0 * Double.pi / self.synth.fmt.sampleRate)
				let carrierVelocity		= self.frequency * self.bend * unitVelocity
				//print("f=\(self.frequency) * b=\(self.bend) -> c=\(carrierVelocity)")

				let modulatorVelocity	= self.modulatorFrequency * unitVelocity	// 2.0

				for sampleIndex in 0 ..< Int(self.samplesPerBuffer) {
					self.phase			+= carrierVelocity
					let mod				= 2 * Float.pi
					self.phase			= self.phase - (self.phase>mod ? mod: 0)

					self.modPhase		+= modulatorVelocity
					self.modPhase		= self.modPhase - (self.modPhase>mod ? mod: 0)
					let fmShift			= self.modulatorAmplitude * sin(self.modPhase)
//					let fmShift			= self.modulatorAmplitude * sin(modulatorVelocity * self.time)
					let value			= sin(self.phase + fmShift)
//					let value			= sin(carrierVelocity * self.time + fmShift)
					let value2			= value * self.ramp

					leftChannel? [sampleIndex] = value2
					rightChannel?[sampleIndex] = value2

					self.time			= self.time + 1.0
					self.ramp			= min(1.0, max(0.0, self.ramp + dRamp))
				}
				//print("buffer \(self.bufferIndex) pushing:\(self.pushing) ramp:\(x) --> \(self.ramp)")
				
				 // Send it off to be played:
				self.player.scheduleBuffer(audioBuffer) {//self.player.scheduleBuffer(audioBuffer, at:nil, options:[.interruptsAtLoop]) {
					//print("buffer done")
					self.playNextBuffer.signal()
				}

				audioBuffer.frameLength = self.samplesPerBuffer
				self.bufferIndex = (self.bufferIndex + 1) % self.bufferPool.count
			}
		}
	}
	func silence() {
		self.pushing = false			// stop pushing on piano's hammar
	}
}
